<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the <div id="wrapper"> and all content
 * after.
 *
 */
?>

	<footer>
	<div id="footer">
		<div id="colophon">
			&copy; <?php echo date("Y"); ?> <a href="<?php bloginfo('url'); ?>" title="Home"><?php bloginfo('name'); ?></a>
			&nbsp;|&nbsp;
			<img src="<?php bloginfo('template_directory'); ?>/images/logo_zenoweb.png" alt="Logo ZenoWeb" />
			&nbsp;
			<a href="http://zenoweb.nl" target="new" >Webdesign Bureau ZenoWeb</a>
			&nbsp;|&nbsp;
			<a href="<?php bloginfo('url'); ?>/disclaimer">disclaimer</a>
		</div><!-- #colophon -->
	</div><!-- #footer -->
	</footer>

</div><!-- #wrapper -->

<?php wp_footer(); ?>


<script type="text/javascript">
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

	ga('create', 'UA-XXXXXXXX-X', 'example.nl'); // pas de code aan naar de echte code, dus de GA-code en domeinnaam
	ga('send', 'pageview');
	ga('set', 'anonymizeIp', true); // we anonimiseren de gegevens
</script>

</body>
</html>
