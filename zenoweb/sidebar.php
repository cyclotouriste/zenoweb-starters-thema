<?php
/**
 * The Sidebar containing the primary and secondary widget areas.
 *
 */
?>

<div id="primary" class="widget-area">

	<?php dynamic_sidebar( 'primary-widget-area' ); ?>

</div><!-- #primary .widget-area -->

