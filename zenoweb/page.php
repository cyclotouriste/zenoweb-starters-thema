<?php
/**
 * The template for displaying all pages.
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages
 * and that other 'pages' on your WordPress site will use a
 * different template.
 *
 */

get_header(); ?>

	<div id="container">
		<div id="content" class="narrowcolumn">
			<article>
			<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<div class="post" id="post-<?php the_ID(); ?>">
			<h2><?php the_title(); ?></h2>
				<div class="entry">
					<?php the_content(); ?>

				</div> <!-- entry -->
			</div> <!-- post -->
			<?php endwhile; endif; ?>
			<?php edit_post_link( __( 'Edit', 'zenoweb' ), '<p>', '</p>'); ?>

			<?php comments_template(); ?>
			</article>
		</div><!-- #content -->

		<?php get_sidebar(); ?>

	</div><!-- #container -->

<?php get_footer(); ?>
