<?php
/*
 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
 *
 * When I wrote this, only God and I understood what I was doing
 * Now, God only knows
 */


define('DISALLOW_FILE_EDIT', true );

function zenoweb_setup() {

	/*
	 * Make theme available for translation.
	 * Translations can be filed in the /languages/ directory.
	 */
	load_theme_textdomain( 'zenoweb', get_template_directory() . '/languages' );


	// This theme styles the visual editor with editor-style.css to match the theme style.
	add_editor_style();

	// Let WordPress manage the document title. Since WP 4.1
	add_theme_support( 'title-tag' );

	// This theme uses post thumbnails
	add_theme_support( 'post-thumbnails' );

	// Switches default core markup for search form, comment form, and comments
	// to output valid HTML5.
	add_theme_support( 'html5', array( 'search-form', 'comment-form', 'comment-list', 'gallery', 'caption' ) );

	/*
	 * This theme supports all available post formats by default.
	 * See http://codex.wordpress.org/Post_Formats
	 */
	add_theme_support( 'post-formats', array(
		'aside', 'audio', 'chat', 'gallery', 'image', 'link', 'quote', 'status', 'video'
	) );

	// Add default posts and comments RSS feed links to head
	add_theme_support( 'automatic-feed-links' );

	// This theme uses wp_nav_menu() in one location.
	register_nav_menus( array(
		'primary' => __( 'Primary Menu', 'zenoweb' )
	) );
}
add_action( 'after_setup_theme', 'zenoweb_setup' );


/*
 * Enqueue scripts and styles.
 */
function zenoweb_scripts() {
	wp_enqueue_style( 'zenoweb-style', get_stylesheet_uri(), false, 2.5, 'all' );

	wp_enqueue_script( 'zenoweb-navigation', get_template_directory_uri() . '/js/navigation.js', array(), '20120206', true );
	wp_enqueue_script( 'zenoweb-frontend', get_template_directory_uri() . '/js/frontend.js', array(), '2.5', true );

	wp_enqueue_script( 'jquery' );

	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
}
add_action( 'wp_enqueue_scripts', 'zenoweb_scripts' );


/*
 *  Register and Enqueue local Open Sans
 */
function zenoweb_admin_scripts() {
	wp_register_style( 'zenoweb-admin', get_template_directory_uri() . '/admin-css/admin.css', false, '2.5', 'all' );
	wp_enqueue_style( 'zenoweb-admin' );
}
add_action( 'admin_enqueue_scripts', 'zenoweb_admin_scripts' );


/*
 * Sets the post excerpt length to 40 characters.
 */
function zenoweb_excerpt_length( $length ) {
	return 40;
}
add_filter( 'excerpt_length', 'zenoweb_excerpt_length' );


/*
 * Returns a "Continue Reading" link for excerpts
 */
function zenoweb_continue_reading_link() {
	return ' <a href="'. get_permalink() . '">' . __( 'Read more...', 'zenoweb' ) . '</a>';
}
/*
 * Replaces "[...]" (appended to automatically generated excerpts) with an ellipsis and zenoweb_continue_reading_link().
 */
function zenoweb_auto_excerpt_more( $more ) {
	return ' &hellip; ' . zenoweb_continue_reading_link();
}
add_filter( 'excerpt_more', 'zenoweb_auto_excerpt_more' );

/*
 * Adds a pretty "Continue Reading" link to custom post excerpts.
 */
function zenoweb_custom_excerpt_more( $output ) {
	if ( has_excerpt() && ! is_attachment() ) {
		$output .= zenoweb_continue_reading_link();
	}
	return $output;
}
add_filter( 'get_the_excerpt', 'zenoweb_custom_excerpt_more' );


/*
 * Get our wp_nav_menu() fallback, wp_page_menu(), to show a home link.
 */
function zenoweb_page_menu_args( $args ) {
	$args['show_home'] = true;
	return $args;
}
add_filter( 'wp_page_menu_args', 'zenoweb_page_menu_args' );


/*
 * Remove the editor from the backend
 */
add_action('admin_init', 'zenoweb_remove_menu_elements', 102);
function zenoweb_remove_menu_elements() {
	remove_submenu_page( 'themes.php', 'theme-editor.php' );
	remove_submenu_page( 'plugins.php', 'plugin-editor.php' );
}


/* Template for comments and pingbacks.
 *
 * Used as a callback by wp_list_comments() for displaying the comments.
 */
function zenoweb_comment( $comment, $args, $depth ) {
	$GLOBALS['comment'] = $comment;
	switch ( $comment->comment_type ) :
		case '' :
		case 'comment' :
	?>
	<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>">
		<div id="comment-<?php comment_ID(); ?>">
			<div class="comment-author vcard">
				<?php echo get_avatar( $comment, 40 ); ?>
				<?php printf( __( '%s <span class="says">writes:</span>', 'zenoweb' ), sprintf( '<cite class="fn">%s</cite>', get_comment_author_link() ) ); ?>
			</div><!-- .comment-author .vcard -->
			<?php if ( $comment->comment_approved == '0' ) : ?>
				<em class="comment-awaiting-moderation"><?php _e( 'Your comments awaits moderation.', 'zenoweb' ); ?></em>
				<br />
			<?php endif; ?>

			<div class="comment-meta commentmetadata"><a href="<?php echo esc_url( get_comment_link( $comment->comment_ID ) ); ?>">
				<?php
				/* translators: 1: date, 2: time */
				printf( __( '%1$s at %2$s', 'zenoweb' ), get_comment_date(),  get_comment_time() ); ?></a><?php edit_comment_link( __( '(Edit)', 'zenoweb' ), ' ' );
				?>
			</div><!-- .comment-meta .commentmetadata -->

			<div class="comment-body"><?php comment_text(); ?></div>

			<div class="reply">
				<?php comment_reply_link( array_merge( $args, array( 'depth' => $depth, 'max_depth' => $args['max_depth'] ) ) ); ?>
			</div><!-- .reply -->
		</div><!-- #comment-##  -->

	<?php
			break;
		case 'pingback'  :
		case 'trackback' :
	?>
	<li class="post pingback">
		<p><?php _e( 'Pingback:', 'zenoweb' ); ?> <?php comment_author_link(); ?><?php edit_comment_link( __( '(Edit)', 'zenoweb' ), ' ' ); ?></p>
	<?php
			break;
	endswitch;
}



/*
 * Register widgetized areas.
 */
function zenoweb_widgets_init() {
	// Area 1, located at the top of the sidebar.
	register_sidebar( array(
		'name' => __( 'Primary Widget Area', 'zenoweb' ),
		'id' => 'primary-widget-area',
		'description' => __( 'The Primary Widget Area', 'zenoweb' ),
		'before_widget' => '<section><div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div></section>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );

	// Area 2, located below the Primary Widget Area in the sidebar. Empty by default.
	register_sidebar( array(
		'name' => __( 'Secondary Widget Area', 'zenoweb' ),
		'id' => 'secondary-widget-area',
		'description' => __( 'The Secondary Widget Area', 'zenoweb' ),
		'before_widget' => '<section><div id="%1$s" class="widget-container %2$s">',
		'after_widget' => '</div></section>',
		'before_title' => '<h3 class="widget-title">',
		'after_title' => '</h3>',
	) );
}
add_action( 'widgets_init', 'zenoweb_widgets_init' );


function zenoweb_headers() {
	// Always use the latest IE mode. Needs to be tested still...
	header("X-UA-Compatible: IE=Edge");
}
add_action( 'after_setup_theme', 'zenoweb_headers' );


