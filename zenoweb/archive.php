<?php
/**
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 */

get_header(); ?>

	<div id="container">
		<div id="content" class="narrowcolumn" role="main">

			<?php
			if (have_posts()) :

				if ( function_exists('the_archive_title') ) {
					the_archive_title( '<h2 class="pagetitle">', '</h2>' );
				}

				while (have_posts()) : the_post(); ?>
					<article>
					<div <?php post_class() ?>>
						<h3 id="post-<?php the_ID(); ?>"><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e( 'Permanent link to', 'zenoweb' ); ?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h3>
						<small><?php the_time('d, M, Y') ?></small>

						<div class="entry">
							<?php the_content() ?>
						</div>

						<p class="postmetadata"><?php the_tags('Tags: ', ', ', '<br />'); ?> <?php _e( 'Placed in', 'zenoweb' ); ?> <?php the_category(', ') ?> | <?php edit_post_link( __( 'Edit', 'zenoweb' ), '', ' | '); ?>  <?php comments_popup_link( __( 'No comments &#187;', 'zenoweb' ), __( '1 comment &#187;', 'zenoweb' ), __( '% comments &#187;', 'zenoweb' ) ); ?></p>

					</div>
					</article>
				<?php endwhile; ?>

				<nav>
				<div class="navigation">
					<?php
					if ( function_exists('wp_pagenavi') ) {
						wp_pagenavi(); // leuke navigatie
					} else { ?>
						<div class="alignleft"><?php next_posts_link( __( '&laquo; Older posts', 'zenoweb' ) ); ?></div>
						<div class="alignright"><?php previous_posts_link( __( 'Newer posts &raquo;', 'zenoweb' ) ); ?></div>
					<?php } ?>
				</div>
				</nav>
			<?php else :

				_e( '<h2 class="center">No posts found.</h2>', 'zenoweb' );

				get_search_form();

			endif; ?>

		</div><!-- #content -->

		<?php get_sidebar(); ?>

	</div><!-- #container -->

<?php get_footer(); ?>
