<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up till <div id="wrapper" and "header">
 *
 */
?><!DOCTYPE html>
<!--[if lt IE 7 ]><html <?php language_attributes(); ?> class="no-js ie ie6 lte7 lte8 lte9"><![endif]-->
<!--[if IE 7 ]><html <?php language_attributes(); ?> class="no-js ie ie7 lte7 lte8 lte9"><![endif]-->
<!--[if IE 8 ]><html <?php language_attributes(); ?> class="no-js ie ie8 lte8 lte9"><![endif]-->
<!--[if IE 9 ]><html <?php language_attributes(); ?> class="no-js ie ie9 lte9"><![endif]-->
<!--[if (gt IE 9)|!(IE)]><!--><html <?php language_attributes(); ?> class="no-js"><!--<![endif]-->
<head>
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width"> <!-- mobile -->
<!-- Geen <title> element meer, thema ondersteunt title-tag sinds WP 4.1 -->
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />
<link rel="icon" type="image/png" href="<?php bloginfo( 'template_url' ); ?>/images/favicon.ico" />
<?php
	/* Always have wp_head() just before the closing </head>
	 * tag of your theme, or you will break many plugins, which
	 * generally use this hook to add elements to <head> such
	 * as styles, scripts, and meta tags.
	 */
	wp_head();

	// javascript to add html5 elements to IE6, IE7, IE8 ?>
	<!--[if lt IE 9]>
	<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

<!--
      .........................................................
      .. ____     __   _    _    ___   _       _    __   ___ ..
      ..|_   /  / _ \ |  \ | |  / _ \  \ \ /\ / / / _ \ | __\..
      ..../ /_ |  __/ | | \| | | |_| |  \ V  V / |  __/ | . \..
      .../____| \___| |_|\ __|  \___/    \_/\_/   \___| |___/..
      .........................................................
-->
  <!--                                                         -->
  <!--                                                         -->
  <!--  Design & realisatie                                    -->
  <!--                                                         -->
  <!--  ZenoWeb                                                -->
  <!--                                                         -->
  <!--  website: www.zenoweb.nl                                -->
  <!--  e-mail:  info(at)zenoweb.nl                            -->
  <!--  Copyright <?php echo date("Y"); ?> ZenoWeb                                 -->
  <!--                                                         -->
  <!--                                                         -->

</head>

<body <?php body_class(); ?>>
<div id="wrapper" class="hfeed">
	<header>
	<div id="header">

		<div id="logo">
			<a href="<?php bloginfo('url'); ?>" title="Home"><?php bloginfo('blogname'); ?></a>
		</div>

		<nav id="site-navigation" class="main-navigation" role="navigation">
		<div id="access">
			<?php /*  Allow screen readers / text browsers to skip the navigation menu and get right to the good stuff */ ?>
			<div class="skip-link screen-reader-text"><a href="#content" title="<?php esc_attr_e( 'Skip to content', 'zenoweb' ); ?>"><?php _e( 'Skip to content', 'zenoweb' ); ?></a></div>

			<!-- Toggle button -->
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><?php _e( 'Primary Menu', 'zenoweb' ); ?></button>

			<?php
			// Our navigation menu. If one isn't filled out, wp_nav_menu falls back to wp_page_menu.  The menu assiged to the primary position is the one used.  If none is assigned, the menu with the lowest ID is used.
			// Let op, het menu wat gebruikt wordt moet nog ingesteld worden als Primary menu
			wp_nav_menu( array( 'container_class' => 'menu-header', 'theme_location' => 'primary' ) ); ?>
		</div><!-- #access -->
		</nav>

	</div><!-- #header -->
	</header>
