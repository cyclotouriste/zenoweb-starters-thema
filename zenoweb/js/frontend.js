

/*
 * JavaScript om een hover state (css class) aan te maken voor touchscreens.
 */
jQuery('.menu > li').click(function(){
	jQuery(this).toggleClass('hover');
});


/*
 * Sub-menu komt er langzaam inzetten (niet op mobile).
 */
jQuery('.menu > li').hover(function() {
	var mobile_click = jQuery('#site-navigation').hasClass('toggled');
	if ( mobile_click ) { return; }

	jQuery(this).closest('li').find('>ul').css({
		'opacity': 0,
	}).show().animate({
		'opacity': 1
	}, 400);
}, function() {
	var mobile_click = jQuery('#site-navigation').hasClass('toggled');
	if ( mobile_click ) { return; }

	jQuery(this).closest('li').find('>ul').fadeOut(200, function() {
		jQuery(this).hide();
	});
});

