<?php
/**
 * The template for displaying Comments.
 *
 * The area of the page that contains both current comments
 * and the comment form. The actual display of comments is
 * handled by a callback to zenoweb_comment which is
 * located in the functions.php file.
 *
 * @package WordPress
 * @subpackage ZenoWeb
 * @since ZenoWeb 2.0
 */
?>

<div id="comments">
	<?php
	if ( post_password_required() ) : ?>
			<p class="nopassword"><?php _e( 'This post is password protected. Enter the password to view it.', 'zenoweb' ); ?></p>
		</div><!-- #comments -->
		<?php
		/* Stop the rest of comments.php from being processed,
		 * but don't kill the script entirely -- we still have
		 * to fully load the template.
		 */
		return;
	endif;


	if ( have_comments() ) : ?>
		<h3 id="comments-title">
			<p class="alignleft"><?php comments_number(__( 'No comments &#187;', 'zenoweb' ), __( '1 comment &#187;', 'zenoweb' ), __( '% comments &#187;', 'zenoweb' )); ?></p>
			<p class="alignright"><a href="#respond" title="<?php _e( 'respond', 'zenoweb' ); ?>"><?php _e( 'respond', 'zenoweb' ); ?></a></p>
		</h3>

		<?php
		// we willen de commentaar lijst andersom, bijv. bij een gastenboek.
		// $comments = array_reverse($comments);


		if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
			<div class="navigation">
				<!-- mooie navigatie -->
				<?php paginate_comments_links(); ?>
				<!-- standaard navigatie -->
				<!-- <div class="nav-previous"><?php previous_comments_link( __( '<span class="meta-nav">&larr;</span> Oudere reacties', 'twentyten' ) ); ?></div>
				<div class="nav-next"><?php next_comments_link( __( 'Nieuwere reacties <span class="meta-nav">&rarr;</span>', 'twentyten' ) ); ?></div> -->
			</div>
			<?php
		endif; // check for comment navigation ?>

		<ol class="commentlist">
			<?php
			/* Loop through and list the comments. Tell wp_list_comments()
			 * to use zenoweb_comment() to format the comments.
			 * See zenoweb_comment() in functions.php for more.
			 */
			wp_list_comments( array( 'callback' => 'zenoweb_comment' ) ); ?>
		</ol>

		<?php
		if ( get_comment_pages_count() > 1 && get_option( 'page_comments' ) ) : // Are there comments to navigate through? ?>
			<div class="navigation">
				<!-- mooie navigatie -->
				<?php paginate_comments_links(); ?>
				<!-- standaard navigatie -->
				<!-- <div class="nav-previous"><?php previous_comments_link( __( '<span class="meta-nav">&larr;</span> Oudere reacties', 'zenoweb' ) ); ?></div>
				<div class="nav-next"><?php next_comments_link( __( 'Nieuwere reacties <span class="meta-nav">&rarr;</span>', 'zenoweb' ) ); ?></div> -->
			</div> <!-- .navigation -->
			<?php
		endif; // check for comment navigation ?>

		<?php
		/* If there are no comments and comments are closed, let's leave a little note, shall we?
		 * But we only want the note on posts and pages that had comments in the first place.
		 */
		if ( ! comments_open() && get_comments_number() ) : ?>
			<p class="nocomments"><?php _e( 'Reageren is gesloten.', 'zenoweb' ); ?></p>
			<?php
		endif;

	endif; // end have_comments() ?>

	<?php
	comment_form(); ?>

</div><!-- #comments -->

