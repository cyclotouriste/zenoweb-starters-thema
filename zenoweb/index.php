<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 */

get_header(); ?>

	<div id="container">
		<div id="content" class="narrowcolumn">

		<?php
		if (have_posts()) : ?>

			<?php while (have_posts()) : the_post(); ?>
				<article>
				<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
					<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e( 'Permanent link to', 'zenoweb' ); ?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
					<small><?php the_time('d M Y') ?></small>

					<div class="entry">
						<?php the_content(); ?>
					</div>

					<p class="postmetadata"><?php the_tags('Tags: ', ', ', '<br />'); ?> <?php _e( 'Placed in', 'zenoweb' ); ?> <?php the_category(', ') ?> | <?php edit_post_link( __( 'Edit', 'zenoweb' ), '', ' | '); ?>  <?php comments_popup_link( __( 'No comments &#187;', 'zenoweb' ), __( '1 comment &#187;', 'zenoweb' ), __( '% comments &#187;', 'zenoweb' ) ); ?></p>
				</div>
				</article>
			<?php endwhile; ?>

			<nav>
			<div class="navigation">
				<?php
				if ( function_exists('wp_pagenavi') ) {
					wp_pagenavi(); // nice navigation
				} else { ?>
					<div class="alignleft"><?php next_posts_link( __( '&laquo; Older posts', 'zenoweb' ) ); ?></div>
					<div class="alignright"><?php previous_posts_link( __( 'Newer posts &raquo;', 'zenoweb' ) ); ?></div>
				<?php } ?>
			</div>
			</nav>
			<?php
		else :

			_e( '<h2 class="center">No posts found.</h2>', 'zenoweb' );

			get_search_form();

		endif; ?>

		</div><!-- #content -->

		<?php get_sidebar(); ?>

	</div><!-- #container -->

<?php get_footer(); ?>
