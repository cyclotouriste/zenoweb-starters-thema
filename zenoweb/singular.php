<?php
/**
 * The Template for displaying all single posts.
 *
 */

get_header(); ?>

	<div id="container">
		<div id="content" class="widecolumn">

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
			<article>
			<div <?php post_class() ?> id="post-<?php the_ID(); ?>">
				<h2><?php the_title(); ?></h2>

				<div class="entry">
					<?php the_content(); ?>

					<p class="postmetadata alt">
						<small>
							<?php the_time('d M Y'); ?> <?php _e( 'Placed in', 'zenoweb' ); ?> <?php the_category(', '); ?>.

							<?php
							if ( comments_open() ) {
								// Comments are open
								_e( 'You can <a href="#respond">leave a comment</a>.', 'zenoweb' );
							} elseif ( !comments_open() ) {
								_e( 'Comments are currently closed.', 'zenoweb' );
							}
							edit_post_link(__( 'Edit', 'zenoweb' ),'','.'); ?>
						</small>
					</p>

				</div>
			</div>

			<?php comments_template(); ?>

			</article>
			<?php
		endwhile; else:

			_e( '<h2 class="center">No posts found.</h2>', 'zenoweb' );

		endif; ?>

		</div><!-- #content -->

		<?php get_sidebar(); ?>

	</div><!-- #container -->
<?php get_footer(); ?>
